<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <title>Ввод данных</title>
</head>
<body>
<form:form action="/calculate?size=${listB.size()}" method="post" >
    <div>
    <p>Матрица А: </p>
<table border="1">
<c:forEach var="list2" items="${listA}" varStatus="status">
    <tr>
        <c:forEach var="l2" items="${list2}" varStatus="counter">
            <td><input type="number" min="-100" max="100" step="0.01" name="name${status.index}${counter.index}" value="${l2.value}" /></td>
        </c:forEach>
    </tr>
</c:forEach>
</table>
    </div>
    <div>
    <p>Матрица свободных членов В: </p>
    <table border="1">
        <c:forEach var="b" items="${listB}" varStatus="counterB">
            <tr>
            <td><input type="number" step="0.01" name="b${counterB.index}" value="${b.value}" ></td>
            </tr>
        </c:forEach>
    </table>
    </div>
<div>
    <p>Выберите метод: </p>
    <select name="methodType" >
        <option value="g" >Метод Гаусса</option>
        <option value="ya" >Метод простых итераций</option>
    </select>
</div>
    <div>
<input type="submit" value="Решить">
    </div>
        </form:form>
</body>
</html>
