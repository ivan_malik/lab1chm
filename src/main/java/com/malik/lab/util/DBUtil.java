package com.malik.lab.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {
    private static Connection connection = null;
    //private final static Logger logger = Logger.getLogger(DBUtil.class);

    public static Connection getConnection() {
        if (connection != null) {
            return connection;
        }
        else {
            try {
                Class.forName("org.postgresql.Driver");
                String url = "jdbc:postgresql://localhost:5432/pgdb";
                String user = "pguser";
                String password = "pguser";
                connection = DriverManager.getConnection(url, user, password);
            } catch (ClassNotFoundException e) {
                //logger.error("Problem with loading driver" + e);
            } catch (SQLException e) {
                //logger.error("SQLException had occurred when connection was establishing." + e);
            }
            //logger.info("Connection successful established.");
            return connection;
        }
    }
}
