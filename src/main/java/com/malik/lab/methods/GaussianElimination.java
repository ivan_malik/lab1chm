package com.malik.lab.methods;

import java.io.BufferedWriter;
import java.io.PrintStream;

public class GaussianElimination {

    private static final double EPSILON = 0.001;
    public static double[] solve(double[][] A, double[] B)
    {
        int N = B.length;
        for (int k = 0; k < N; k++)
        {

            int max = k;
            for (int i = k + 1; i < N; i++) {
                if (Math.abs(A[i][k]) > Math.abs(A[max][k])) {
                    max = i;
                }
            }

            double[] temp = A[k];
            A[k] = A[max];
            A[max] = temp;

            double t = B[k];
            B[k] = B[max];
            B[max] = t;

            if (Math.abs(A[k][k]) <= EPSILON) {
                throw new ArithmeticException("Matrix is singular or nearly singular");
            }

            printRowEchelonForm(A, B);

            for (int i = k + 1; i < N; i++)
            {
                double factor = A[i][k] / A[k][k];
                System.out.println("умножим " + (k+1) + " строку на " + factor + "и отнимем от " + (i+1));
                B[i] -= factor * B[k];
                for (int j = k; j < N; j++) {
                    A[i][j] -= factor * A[k][j];
                }
                printRowEchelonForm(A, B);
            }


        }
        System.out.println("------------------------------------------");
        printRowEchelonForm(A, B);

        double[] solution = new double[N];
        for (int i = N - 1; i >= 0; i--)
        {
            double sum = 0.0;
            for (int j = i + 1; j < N; j++)
                sum += A[i][j] * solution[j];
            solution[i] = (B[i] - sum) / A[i][i];
        }
        printSolution(solution);

        return solution;
    }

    private static void printRowEchelonForm(double[][] A, double[] B)
    {
        int N = B.length;
        System.out.println("\nRow Echelon form : ");
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
                System.out.printf("%.3f ", A[i][j]);
            System.out.printf("| %.3f\n", B[i]);
        }
        System.out.println();
    }

    private static void printSolution(double[] sol)
    {
        int N = sol.length;
        System.out.println("\nSolution : ");
        for (double aSol : sol) System.out.printf("%.3f ", aSol);
        System.out.println();
    }
}
