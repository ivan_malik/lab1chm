package com.malik.lab;

public class Value {
    private double value;

    public Value() {
    }

    public Value(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
