package com.malik.lab.controller;

import com.malik.lab.Dao.UserDao;
import com.malik.lab.Value;
import com.malik.lab.methods.GaussianElimination;
import com.malik.lab.methods.JacobiElimination;
import com.malik.lab.model.User;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Controller
public class StartController {
    @RequestMapping(value = "/")
    public String start() {
        return "index";
    }

    @RequestMapping(value = "/help")
    public ModelAndView help() {
        ModelAndView modelAndView = new ModelAndView("help");
        List<User> list = new UserDao().getAllUsers();
        modelAndView.addObject("list", list);
        return modelAndView;
    }

    @RequestMapping(value = "/tomethod", method = RequestMethod.POST)
    public ModelAndView meth(HttpServletRequest request){
        int variablesCount = Integer.parseInt(request.getParameter("variablesCount"));
        ArrayList<ArrayList<Value>> list = new ArrayList<>(variablesCount);
        for (int i = 0; i < variablesCount; i++) {
            ArrayList<Value> list1 = new ArrayList<>();
            for (int j = 0; j < variablesCount; j++) {
                list1.add(new Value(0));
            }
            list.add(list1);
        }
        ArrayList<Value> listB = new ArrayList<>();
        for (int i = 0; i < variablesCount; i++)
            listB.add(new Value(0));
        ModelAndView modelAndView = new ModelAndView("method");
        modelAndView.addObject("variablesCount", variablesCount);
        modelAndView.addObject("listA", list);
        modelAndView.addObject("listB", listB);
        return modelAndView;
    }

    @RequestMapping("/calculate")
    public ModelAndView result(HttpServletRequest request) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(new File("C:/Users/DEDUSHKA/IdeaProjects/Lab1ChMSpring/src/main/resources/report.html")));
        BufferedWriter bwi = new BufferedWriter(new FileWriter(new File("C:/Users/DEDUSHKA/IdeaProjects/Lab1ChMSpring/src/main/resources/input.txt")));
        ModelAndView modelAndView = new ModelAndView("result");
        int size = Integer.parseInt(request.getParameter("size"));
        double[] res = new double[size];
        ArrayList<String> reslist = new ArrayList<>();
        double[][] A = new double[size][size];
        double[] B = new double[size];
        bw.append("<p>" + "Matrix A: " + "\n" + "</p>");
        bw.append("<dl>");
        for(int i = 0; i < size; i++) {
            bw.append("<dt>");
            for(int j = 0; j < size; j++) {
                A[i][j] = Double.parseDouble(request.getParameter("name" + i+"" + j+""));
                bw.append(String.valueOf(A[i][j])).append("x").append(String.valueOf(j + 1)).append(" ");
            }
            bw.append("</dt>");
        }
        bw.append("</dl>");
        bw.append("<p>" + "Result matrix B: " + "\n" + "</p>");
        for (int i = 0; i < size; i++) {
            B[i] = Double.parseDouble(request.getParameter("b" + i));
            bw.append(String.valueOf(B[i])).append("<br/>");
        }
        for(int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++)
                bwi.append(String.valueOf(A[i][j])).append(" ");
            bwi.append(String.valueOf(B[i])).append("\n");
        }
        String meth = request.getParameter("methodType");
        if (meth.equals("g")) {
            bw.append("<h3>Solved by Gaussian Elimination</h3> \n");
            res = GaussianElimination.solve(A, B);
        }
        else {
            bw.append("<h3>Solved by Jacobi Elimination</h3> \n");
            JacobiElimination.solve(size, A, B, res);
        }
        bw.write("<p>Result: </p>");
        if(res.length != 0) {
            for (int i = 0; i < res.length; i++) {
                reslist.add(String.format("%.3f", res[i]));
                bw.append("x").append(String.valueOf(i + 1)).append(" = ").append(String.valueOf(res[i])).append(" ");
            }
        }
        modelAndView.addObject("res", reslist);
        bw.close();
        bwi.close();
        return modelAndView;
    }

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public String submit(@RequestParam("file") MultipartFile file, ModelMap modelMap) {
        BufferedReader br;
        ArrayList<ArrayList<Value>> listA = new ArrayList<>();
        ArrayList<Value> listB = new ArrayList<>();
        try {
            String line;
            InputStream is = file.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                String[] temp = line.split(" ");
                ArrayList<Value> tempList = new ArrayList<>();
                for (int i = 0; i < temp.length-1; i++)
                    tempList.add(new Value(Double.parseDouble(temp[i])));
                listA.add(tempList);
                listB.add(new Value(Double.parseDouble(temp[temp.length-1])));
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        modelMap.addAttribute("listA", listA);
        modelMap.addAttribute("listB", listB);
        return "method";
    }

    @RequestMapping("/download")
    public ResponseEntity<InputStreamResource> downloadReport() throws IOException {
        File file = new File("C:/Users/DEDUSHKA/IdeaProjects/Lab1ChMSpring/src/main/resources/report.html");
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment;filename=" + file.getName())
                .contentType(MediaType.TEXT_PLAIN).contentLength(file.length())
                .body(resource);
    }

    @RequestMapping("/downloadI")
    public ResponseEntity<InputStreamResource> downloadInputData() throws IOException {
        File file = new File("C:/Users/DEDUSHKA/IdeaProjects/Lab1ChMSpring/src/main/resources/input.txt");
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment;filename=" + file.getName())
                .contentType(MediaType.TEXT_PLAIN).contentLength(file.length())
                .body(resource);
    }
}
